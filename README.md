README
================

<!-- README.md is generated from README.Rmd. Please edit that file -->

This is inline math $`a ^ 2 + b ^ 2 = c ^ 2`$.

``` math
a ^ 2 + b ^ 2 = c ^ 2
```
